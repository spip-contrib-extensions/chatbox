<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_chatbox_message' => 'Ajouter ce message',

	// E
	'explication_id_auteur' => 'L\'auteur du message',
	'explication_message' => 'Votre message',

	// I
	'icone_creer_chatbox_message' => 'Créer un message',
	'icone_modifier_chatbox_message' => 'Modifier ce message',
	'info_1_chatbox_message' => 'Un message',
	'info_aucun_chatbox_message' => 'Aucun message',
	'info_chatbox_messages_auteur' => 'Les messages de cet auteur',
	'info_nb_chatbox_messages' => '@nb@ messages',

	// L
	'label_id_auteur' => 'Auteur',
	'label_message' => 'Message',

	// R
	'retirer_lien_chatbox_message' => 'Retirer ce message',
	'retirer_tous_liens_chatbox_messages' => 'Retirer tous les messages',

	// T
	'texte_ajouter_chatbox_message' => 'Ajouter un message',
	'texte_changer_statut_chatbox_message' => 'Ce message est :',
	'texte_creer_associer_chatbox_message' => 'Créer et associer un message',
	'titre_chatbox_message' => 'Chatbox message',
	'titre_chatbox_messages' => 'Chatbox messages',
	'titre_chatbox_messages_rubrique' => 'Messages de la rubrique',
	'titre_langue_chatbox_message' => 'Langue de ce message',
	'titre_logo_chatbox_message' => 'Logo de ce message',
);

?>