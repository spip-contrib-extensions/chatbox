<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chatbox_description' => 'Une chatbox permettant aux visiteurs de laisser un message rapidement.',
	'chatbox_nom' => 'Chatbox',
	'chatbox_slogan' => 'Une chatbox très simple d\'utilisation',
);

?>