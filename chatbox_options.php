<?php
/**
 * Plugin Chatbox
 * (c) 2013 g0uZ
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/*
 * Un fichier d'options permet de definir des elements
 * systematiquement charges à chaque hit sur SPIP.
 *
 * Il vaut donc mieux limiter au maximum son usage
 * tout comme son volume !
 * 
 */

?>